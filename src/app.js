const stripe = require("stripe")(
  "sk_test_51Kg7FSHhD5Q81mjduVGOt9ZCvN39vtXrKGfYSbBKGs13BnOSS3FDvWPULfl86nJIuxDkpO4algTgEhoEXDw1tpLC00zoDPJ0dY"
);
const express = require("express");
const cors = require("cors");
const app = express();

app.use(express.json());
app.use(cors({ origin: true }));

//GET
app.get("/", (req, res) => {
  res.status(200).send("This is HomePlus stripe API");
});

//POST
app.post("/create-payment-intent", async (req, res) => {
  const { id, amount } = req.body;

  try {
    await stripe.paymentIntents.create({
      amount,
      currency: "aud",
      payment_method: id,
      confirm: true,
    });

    return res.status(200).json("Task fee has been paid");
  } catch (error) {
    return res.status(400).json(error);
  }
});

module.exports = app;
